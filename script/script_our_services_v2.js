// const nameCssActive = 'active' // initialize in script.js

// section: Our Services
// css class: .main-our-services

const tabsMenuItemLink = 'tabs-menu__item-link';
const tabsMenu = document.querySelector('.our-services__tabs-menu');
const tabsContent = document.querySelector('.our-services__tabs-content');

tabsMenu.addEventListener('click', changeTabsMenuItemActive);

function changeTabsMenuItemActive(e) {
  const item = e.target;
  if (
    item.classList.contains(tabsMenuItemLink) &&
    !item.classList.contains(nameCssActive)
  ) {
    clearStateActive(tabsMenu.children);
    setStateActive(item);
    changeTabsContentItemActive(item);
  }
}

function changeTabsContentItemActive(item) {
  const contentItem = getTabsConnectedContent(item);

  tabsMenu.style.pointerEvents = 'none';
  for (let i of tabsContent.children) {
    if (i.classList.contains(nameCssActive)) {
      i.style.opacity = 0;
      i.style.top = -40 + 'px';

      setTimeout(() => {
        i.classList.remove(nameCssActive);
        i.style.top = 0 + 'px';
      }, 400);
    }
  }

  if (contentItem) {
    contentItem.style.top = 40 + 'px';
    contentItem.style.opacity = 0;

    setTimeout(() => {
      contentItem.style.top = 0 + 'px';
      contentItem.style.opacity = 1;
      contentItem.classList.add(nameCssActive);
      tabsMenu.removeAttribute('style');
    }, 200);
  } else {
    alert('Error: not finded connectedContentItem');
  }
}

function getTabsConnectedContent(item) {
  for (let i of tabsContent.children) {
    if (i.dataset.tabsItemLink === item.dataset.tabsMenuLink) {
      return i;
    }
  }
  return false;
}
