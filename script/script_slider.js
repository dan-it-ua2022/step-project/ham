// const nameCssActive = 'active' // initialize in script.js

// section: What People Say About theHam
// css class: .main-about

const carouselItemsFoto = document.querySelector('.carousel__items-foto');
const itemFotoAll = document.querySelectorAll(
  '.carousel__items-foto .item-foto'
);
const sliderItems = document.querySelector('.about-slider__items');
const prevBtn = document.querySelector('.carousel__prev');
const nextBtn = document.querySelector('.carousel__next');

const carouselItemsFotoArray = [];
let indexCarouselFotoActive;
const indexesCarouselFotos = [];
const countAddFotoCarousel = 4;
const valuePxMove = 80;
const valuePxDefault = -160;

function initSlider() {
  for (let i = 0; i < itemFotoAll.length; i++) {
    carouselItemsFotoArray.push(itemFotoAll[i]);
    if (itemFotoAll[i].classList.contains(nameCssActive)) {
      indexCarouselFotoActive = i;
    }
    itemFotoAll[i].remove();
  }
  setIndexesCarouselFotos();
}

function setIndexesCarouselFotos() {
  indexesCarouselFotos.length = 0;
  carouselItemsFoto.innerHTML = '';
  for (
    let i = indexCarouselFotoActive - countAddFotoCarousel;
    i <= indexCarouselFotoActive + countAddFotoCarousel;
    i++
  ) {
    let item;
    if (i < 0) {
      item = carouselItemsFotoArray.length + i;
    }
    if (i >= 0 && i < carouselItemsFotoArray.length) {
      item = i;
    }
    if (i >= carouselItemsFotoArray.length) {
      item = i - carouselItemsFotoArray.length;
    }
    indexesCarouselFotos.push(item);
    carouselItemsFoto.append(carouselItemsFotoArray[item]);
  }
}

function handlerCarouselFoto(e) {
  const element = e.target.closest('.item-foto');
  if (element) {
    if (element.classList.contains(nameCssActive)) {
      return;
    }

    const step =
      indexesCarouselFotos.indexOf(Number(element.dataset.item)) -
      countAddFotoCarousel;

    handlerMoveCarousel(-step, step);
  } else {
    alert('Error: not element in handlerCarouselFoto(e)');
  }
}

function handlerMoveCarousel(count, step) {
  // prevBtn.style.pointerEvents = 'none';
  // nextBtn.style.pointerEvents = 'none';
  carouselItemsFoto.style.transition = 'all ease 0.3s';
  carouselItemsFoto.style.transform = `translateX(${
    valuePxDefault + valuePxMove * count
  }px)`;
  indexCarouselFotoActive = indexesCarouselFotos[countAddFotoCarousel + step];
  clearStateActive(carouselItemsFotoArray);
  setStateActive(carouselItemsFotoArray[indexCarouselFotoActive]);

  setTimeout(() => {
    // prevBtn.removeAttribute('style');
    // nextBtn.removeAttribute('style');
    carouselItemsFoto.removeAttribute('style');
    setIndexesCarouselFotos();
    setActiveContent(carouselItemsFotoArray[indexCarouselFotoActive]);
  }, 350);
}

function setActiveContent(item) {
  const contentItem = getInfoConnectedContent(item);

  if (contentItem) {
    clearStateActive(sliderItems.children);
    setStateActive(contentItem);
  } else {
    alert('Error: not finded connectedContentItem');
  }
}

function getInfoConnectedContent(item) {
  for (let i of sliderItems.children) {
    if (i.dataset.item === item.dataset.item) {
      return i;
    }
  }
  return false;
}

prevBtn.addEventListener('click', () => handlerMoveCarousel(1, -1));
nextBtn.addEventListener('click', () => handlerMoveCarousel(-1, 1));
carouselItemsFoto.addEventListener('click', handlerCarouselFoto);

initSlider();
